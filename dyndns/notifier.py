import hashlib
import json
import logging
import threading
import queue

import backoff
import requests

from prometheus_client import Counter

prom_failed_updates = Counter(
    "dnsclient_failed_updates_total", "Failures while updating the DNS record"
)
prom_update_request_status_code = Counter(
    "dnsclient_update_requests_total", "Status code of request", ["status_code"]
)


class UpdateNotifier(threading.Thread):
    def __init__(self, dns_record, host, shared_secret):
        self._set_dns_record(dns_record)
        super().__init__()
        self.name = "UpdateNotifier"

        if not host:
            raise ValueError("host not specified")
        self.host = host

        if not shared_secret:
            raise ValueError("secret not specified")
        self.shared_secret = shared_secret

        self._queue = queue.Queue()
        self._continue = True

    def _set_dns_record(self, dns_record):
        """
        Set the DNS record. The model requires the record to end with a dot,
        so make sure here it is set.
        """
        if not dns_record:
            raise ValueError("dns_record not specified")

        if not dns_record.endswith("."):
            dns_record += "."
        self.dns_record = dns_record

    def interrupt(self):
        logging.info("Stopping updating DNS entries...")
        self._continue = False

    def notify_update(self, fetched_ip):
        self._queue.put(fetched_ip)
        logging.info("Adding %s to queue", fetched_ip)

    def run(self):
        #pylint: disable=broad-except
        while self._continue:
            try:
                fetched_ip = self._queue.get(timeout=2)
                logging.info("Picked up new ip from queue: %s", fetched_ip)
                self._send_update(fetched_ip)
            except requests.exceptions.RequestException as err:
                prom_failed_updates.inc()
                logging.error(
                    "Error sending update, received status code %s",
                    err.response.status_code,
                )
            except queue.Empty:
                pass
            except Exception as err:
                prom_failed_updates.inc()
                logging.error("Unknown error while sending update: %s", err)
        logging.info("Stopped updating DNS entries")

    @staticmethod
    def hash_request(host, external_ip, shared_secret):
        """ 'Sign' our request with the shared secret. """
        if not host or not external_ip or not shared_secret:
            raise ValueError("Uninitialized value provided")

        return hashlib.sha256(
            f"{host}{external_ip}{shared_secret}".encode("utf-8")
        ).hexdigest()

    def _build_request(self, external_ip):
        """ Create request object to send to the endpoint. """

        if not external_ip:
            raise ValueError("Can not build request object, external_ip is missing")

        payload = dict()

        payload["validation_hash"] = UpdateNotifier.hash_request(
            self.dns_record, external_ip, self.shared_secret
        )
        payload["dns_record"] = self.dns_record
        payload["public_ip"] = external_ip

        return payload

    @staticmethod
    def _is_fatal(err):
        prom_update_request_status_code.labels(err.response.status_code).inc()
        return 400 <= err.response.status_code < 500

    @backoff.on_exception(
        backoff.constant,
        requests.exceptions.RequestException,
        interval=60,
        giveup=_is_fatal,
    )
    def _send_update(self, fetched_ip):
        """ Notify the server about an updated IP address. """
        logging.info("Sending update to remote server")
        payload = self._build_request(fetched_ip)

        headers = {"Content-type": "application/json", "Accept": "text/plain"}
        response = requests.post(self.host, data=json.dumps(payload), headers=headers)

        prom_update_request_status_code.labels(response.status_code).inc()
        logging.info(
            "Successfully sent update, received status code %s", response.status_code
        )
