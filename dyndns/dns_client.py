import logging
import signal
from inspect import getmembers, isfunction

import configargparse
from prometheus_client import start_http_server

import ipv4_providers
from dyndns_updater import UpdateDetector
from notifier import UpdateNotifier


def read_config():
    """ Parse CLI args. """
    parser = configargparse.ArgumentParser(prog="dns_client")

    parser.add_argument(
        "-u",
        "--url",
        dest="url",
        action="store",
        env_var="DNSCLIENT_URL",
        required=True,
        help="The URL of the server component",
    )
    parser.add_argument(
        "-r",
        "--record",
        dest="record",
        action="store",
        env_var="DNSCLIENT_RECORD",
        required=True,
        help="The full DNS record to update. It should end with a dot",
    )
    parser.add_argument(
        "-s",
        "--secret",
        dest="shared_secret",
        action="store",
        env_var="DNSCLIENT_SECRET",
        required=True,
        help="The secret for the appropriate DNS record",
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        env_var="DNSCLIENT_DEBUG",
        default=False,
        help="Print debug messages",
    )
    parser.add_argument(
        "--prom_port",
        dest="prom_port",
        action="store",
        env_var="DNSCLIENT_PROMPORT",
        type=int,
        default=0,
        help="Start prometheus endpoint at given port. Setting to 0 disables the endpoint. Defaults to 0.",
    )
    parser.add_argument(
        "--prom_addr",
        dest="prom_addr",
        action="store",
        env_var="DNSCLIENT_PROMADDR",
        default="127.0.0.1",
        help="Sets the address for the endpoint to listen on, given the endpoint is enabled. Defaults to '127.0.0.1'.",
    )
    parser.add_argument(
        "-i",
        "--interval",
        dest="interval",
        action="store",
        type=int,
        env_var="DNSCLIENT_INTERVAL",
        default=60,
        help="The interval in seconds to check a random IP provider. Defaults to 60",
    )
    parser.add_argument(
        "-f",
        "--file",
        dest="file",
        action="store",
        env_var="DNSCLIENT_FILE",
        required=False,
        help="Save resolved IP to a file to preserve the status across service restarts.",
    )

    return parser.parse_args()


def print_config(args, ip_providers):
    """ Print configuration after startup """
    logging.info("Using the following parameters")
    logging.info("url=%s", args.url)
    logging.info("record=%s", args.record)
    logging.info("interval=%d", args.interval)
    logging.info("prom_port=%d", args.prom_port)
    logging.info("prom_addr=%s", args.prom_addr)
    if "file" in args:
        logging.info("file=%s", args.file)
    logging.info("providers=%s", [x[0] for x in ip_providers])


def init_logging(debug=False):
    """ Setup logging """
    loglevel = logging.INFO
    if debug:
        loglevel = logging.DEBUG
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s", level=loglevel
    )
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def get_ipv4_providers():
    """ Return all configured IP providers """
    logging.info("Loading IP providers")
    return [f for f in getmembers(ipv4_providers, isfunction)]


def prometheus_server(args):
    if args.prom_port < 1:
        logging.info("Not starting prometheus metrics endpoint")
        return

    logging.info(
        "Start prometheus metrics endpoint at %s:%d", args.prom_addr, args.prom_port
    )
    start_http_server(port=args.prom_port, addr=args.prom_addr)


class DnsUpdater:
    def __init__(self, args, providers):
        if not args:
            raise ValueError()

        self.args = args
        self.providers = providers

        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)

        self.notifier = UpdateNotifier(
            dns_record=self.args.record,
            host=self.args.url,
            shared_secret=self.args.shared_secret,
        )

        self.detector = UpdateDetector(
            update_notifier=self.notifier,
            ip_providers=self.providers,
            interval=self.args.interval,
            dns_record=self.args.record,
        )

    def signal_handler(self, sig, frame):
        # pylint: disable=unused-argument
        logging.info("Received sigint, starting graceful shutdown")
        self.notifier.interrupt()
        self.detector.interrupt()
        self.notifier.join()

    def initialize(self):
        """ Start up """
        self.notifier.start()
        self.detector.start()
        logging.info("Bye!")


def doit():
    args = read_config()

    init_logging(args.debug)
    ip_providers = get_ipv4_providers()

    print_config(args, ip_providers)
    prometheus_server(args)
    DnsUpdater(args, ip_providers).initialize()


if __name__ == "__main__":
    doit()
