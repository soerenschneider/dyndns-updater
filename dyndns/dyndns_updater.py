import random
import logging
import ipaddress
import time
import socket

import backoff
import requests
from prometheus_client import Counter, Gauge

prom_ipresolver_status = Counter(
    "dnsclient_ipresolver_count_total",
    "Amount of calls for external IP resolving",
    ["site", "status_code"],
)
prom_ipresolver_failed = Counter(
    "dnsclient_ipresolver_failed_total", "Amount of failed external IP discoverings"
)
prom_last_check = Gauge(
    "dnsclient_last_check_ts_seconds", "Timestamp of the latest check for a new IP"
)
prom_update_detected = Counter(
    "dnsclient_updates_detected_total", "Amount of IP updates detected"
)
prom_update_detected_ts = Gauge(
    "dnsclient_last_detected_update_ts_seconds", "Timestamp of update"
)
prom_backend_errors = Counter(
    "dnsclient_backend_errors_total",
    "Errors with backend interaction",
    ["operation", "backend_name"],
)


class UpdateDetector:
    def __init__(self, update_notifier, ip_providers, dns_record=None, interval=None):
        if not update_notifier:
            raise ValueError("No update_notifier configured")
        self.update_notifier = update_notifier

        if not ip_providers:
            raise ValueError("No providers for determing IP address found")
        self.ip_providers = ip_providers

        if dns_record:
            self.dns_record = dns_record

        if not interval:
            interval = 60
        self.interval = interval

        self._continue = True

    @staticmethod
    def shuffle_providers(providers):
        """ Makes sure we're using the providers more or less evenly. """
        if providers:
            random.shuffle(providers)

    @staticmethod
    def is_valid_ipv4(ip: str) -> bool:
        #pylint: disable=invalid-name
        """ Check whether the supplied argument is a valid IPv4 address """
        if not ip:
            return False

        try:
            parsed = ipaddress.ip_interface(ip)
            return isinstance(parsed, ipaddress.IPv4Interface)
        except ValueError:
            return False

    @staticmethod
    @backoff.on_exception(
        backoff.expo, requests.exceptions.RequestException, max_tries=3
    )
    def request_wrapper(provider_function):
        """
        Wrapper using a backoff annotation that just executes the function to fetch data
        from IP provider.
        """
        if not provider_function:
            raise ValueError("No function to call provided")

        return provider_function()

    def get_external_ip(self):
        #pylint: disable=broad-except
        """ Iterate all IP providers until the first one gives a valid response. """
        prom_last_check.set_to_current_time()

        for provider in self.ip_providers:
            try:
                external_ip, status_code = UpdateDetector.request_wrapper(provider[1])
                prom_ipresolver_status.labels(provider[0], status_code).inc()
                if external_ip:
                    external_ip = external_ip.strip()
                    # before proceeding make sure this provider didn't provide garbage
                    if (
                        UpdateDetector.is_valid_ipv4(external_ip) is True
                        and status_code < 400
                    ):
                        return external_ip
            except Exception as err:
                logging.debug(
                    "Failed to fetch information from provider '%s': %s",
                    provider[0],
                    err,
                )

        prom_ipresolver_failed.inc()
        logging.error("Giving up after all providers failed: Is the network down?")
        return None

    @staticmethod
    def has_update_occured(last_ips, fetched_ip):
        """ Returns True when a IP change has been detected, otherwise False. """
        logging.debug("Checking if %s is in %s", fetched_ip, last_ips)
        if fetched_ip and fetched_ip not in last_ips:
            logging.info("Detected new IP: %s", fetched_ip)
            prom_update_detected.inc()
            prom_update_detected_ts.set_to_current_time()
            return True

        logging.debug("No update detected")
        return False

    @staticmethod
    def resolve_host(host):
        try:
            return list({i[4][0] for i in socket.getaddrinfo(host, None)})
        except OSError:
            return list()

    def perform_check(self, last_ips):
        self.shuffle_providers(self.ip_providers)
        fetched_ip = self.get_external_ip()

        if self.has_update_occured(last_ips, fetched_ip) is True:
            self.update_notifier.notify_update(fetched_ip)

        return [fetched_ip]

    def interrupt(self):
        logging.info("Stopping detection of new IPs")
        self._continue = False

    def start(self):
        #pylint: disable=broad-except
        logging.info("Started!")

        last_ips = None
        if self.dns_record:
            last_ips = UpdateDetector.resolve_host(self.dns_record)

        logging.info("Resolved %s via DNS lookup", last_ips)

        while self._continue:
            try:
                last_ips = self.perform_check(last_ips)
            except Exception as err:
                logging.error("Error while updating: %s", err)
            time.sleep(self.interval)
        logging.info("IP detection stopped")
